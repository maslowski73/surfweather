package mm.sonalake.surfweather;

import com.fasterxml.jackson.annotation.JsonView;

public class PlaceToSurf 
{
    @JsonView(SurfWeatherApplication.BasicView.class)
    private final String winnerPlace;
    @JsonView(SurfWeatherApplication.BasicView.class)
    private final SurfWeather winnerWeather;
    
    public PlaceToSurf() {
        super();
        this.winnerPlace = "";
        this.winnerWeather = null;
    }

    public PlaceToSurf(String winnerPlace, SurfWeather winnerWeather) {
        super();
        this.winnerPlace = winnerPlace;
        this.winnerWeather = winnerWeather;
    }
    
}
