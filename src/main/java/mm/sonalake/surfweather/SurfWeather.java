package mm.sonalake.surfweather;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Class representing weather parameters for surfing
 * @author mario
 *
 */
public class SurfWeather 
{
    @JsonView(SurfWeatherApplication.DetailedView.class)
    private Double windSpeed;
    @JsonView(SurfWeatherApplication.DetailedView.class)
    private Double tempLow;
    @JsonView(SurfWeatherApplication.DetailedView.class)
    private Double tempHigh;
    
    @JsonView(SurfWeatherApplication.BasicView.class)
    Map<String, Object> dsDailyJson;
    
    public SurfWeather(Map<String, Object> darkSkyJson) 
    {
        this.dsDailyJson = darkSkyJson;
        this.windSpeed = (Double) darkSkyJson.get("windSpeed");
        this.tempLow   = (Double) darkSkyJson.get("temperatureLow");
        this.tempHigh  = (Double) darkSkyJson.get("temperatureHigh");
    }

    /**
     * Jeśli prędkość wiatru nie zawiera się w przedziale <5; 18> (m/s), 
     * a temperatura w przedziale <5; 35> (°C) 
     * to dana lokalizacja nie nadaje się do surfingu
     * 
     * @return true if this location qualifies
     */
    public boolean qualifies() 
    {
        if (5 < windSpeed && windSpeed < 18 &&
            5 < tempLow && tempHigh < 35)
        {
            return true;
        }
        return false;
    }

    /**
     * wygrywa miejsce, które ma wyższą wartość obliczoną ze wzoru:
     *   v * 3 + (tempLow + tempHigh)/2
     * gdzie v, to prędkość wiatru w m/s w danym dniu, 
     * a tempLow i tempHigh, to odpowiednio najniższa i najwyższa temperatura 
     * prognozowana danego dnia w stopniach Celsjuszach.
     * 
     * @return score computed using the equation above
     */
    public double calculateScore() 
    {
        return 3 * windSpeed + (tempLow + tempHigh) / 2;
    }

    
}
