package mm.sonalake.surfweather;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Format;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.validator.constraints.ParameterScriptAssert;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.json.JsonParseException;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.annotation.JsonView;

@SpringBootApplication
@RestController
public class SurfWeatherApplication 
{
    public static class BasicView { }
    public static class DetailedView extends BasicView { }
    
	private static final String DARK_SKY_API_SECRET_KEY = "50fa64eec50458ae1f0e63dad0cf4b34";
	/** https://api.darksky.net/forecast/[key]/[latitude],[longitude],[time] */
	private static final String DARK_SKY_API_TIME_MACHINE_FORMAT = 
			"https://api.darksky.net/forecast/%s/%s,%d?exclude=currently,hourly,flags&units=si";
    
	private static final String PLACES_FILENAME = "places.json";
	private static final String DEFAULT_PLACES[][] = {
			{"Jastarnia", "54.699444,18.676667"},
			{"Chałupy",   "54.758611,18.509444"}
	};

    @JsonView(SurfWeatherApplication.DetailedView.class)
    @GetMapping(path = "/whereToSurf")
//    public Map<String, Object> whereToSurf(@RequestParam String date)
    public PlaceToSurf whereToSurf(@RequestParam String date)
    {
        long time = getNoonSeconds(date);
        double score = 0; 
        String winnerPlace = "";
        SurfWeather winnerWeather = null;
        
        String places[][] = readPlaces();

        for (int i = 0; i < places.length; i++) 
        {
            String placeName   = places[i][0];
            String placeCoords = places[i][1];
            
            String dsUrl = printfToString(
                    DARK_SKY_API_TIME_MACHINE_FORMAT, 
                    DARK_SKY_API_SECRET_KEY, placeCoords, time);
            
            SurfWeather weather = retrieveWeather(dsUrl);
            if (weather != null && weather.qualifies()) 
            {
                double gain = weather.calculateScore();
                if (gain > score)
                {
                    score = gain;
                    winnerPlace = placeName;
                    winnerWeather = weather;
                }
            }
            
        } 
        
        PlaceToSurf placeToSurf = 
                (score > 0)  ? new PlaceToSurf(winnerPlace, winnerWeather)
                        : new PlaceToSurf();
        
		return placeToSurf;
    }

    private String[][] readPlaces() 
    {
        String[][] places;
        try 
        {
            FileInputStream fis = new FileInputStream(PLACES_FILENAME);
            String body = extractBody(fis);
            final JsonParser parser = JsonParserFactory.getJsonParser();
            final List<Object> list = parser.parseList(body);
            
            places = new String[list.size()][];
            for (int i = 0; i < places.length; i++) {
                Map<String, Object> m = (Map<String, Object>) list.get(i);
                String[] place = places[i] = new String[2];
                place[0] = (String) m.get("name");
                place[1] = (String) m.get("coords");
                
            }
        } 
        catch (IOException | JsonParseException e) 
        {
            places = DEFAULT_PLACES;
        }

        return places;
    }


    private SurfWeather retrieveWeather(String urlStr)
    {
//        System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

        try
        {
            final URL url = new URL(urlStr);
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();

            final int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) 
            {
                final String body = extractBody(conn.getInputStream());

                final JsonParser parser = JsonParserFactory.getJsonParser();
                final Map<String, Object> map = parser.parseMap(body);
                if (!map.isEmpty()) 
                {
                    Map<String, Object> daily = (Map<String, Object>) map.get("daily");
                    List<Object> data = (List<Object>) daily.get("data");
                    Map<String, Object> dailyWeather = (Map<String, Object>) data.get(0);

                    return new SurfWeather(dailyWeather);
                }
            }
        }
        catch (Exception e) {
            // TODO: handle exception
        }
        
        return null;
    }
    
    private static String extractBody(final InputStream isr) throws IOException
    {
        final BufferedReader in = new BufferedReader(new InputStreamReader(isr));
        final StringBuffer sb = new StringBuffer();
        String inputLine;
        while ((inputLine = in.readLine()) != null)
        {
            sb.append(inputLine);
        }
        in.close();
        return sb.toString();
    }

    private long getNoonSeconds(String date) 
    {
        LocalDate d;
        try {
            d = LocalDate.parse(date);
        } catch (Exception e) {
            d = LocalDate.now();
        }
        LocalDateTime dt = d.atTime(12, 0);
        long time = dt.atZone(ZoneId.systemDefault()).toEpochSecond();
        return time;
    }
    
    private static String printfToString(String fmt, Object ... args)
    {
        final StringWriter strWriter = new StringWriter();
        final PrintWriter writer = new PrintWriter(strWriter);

        writer.printf(fmt, args);

        return strWriter.toString();
    }

    
    @Configuration
    @EnableWebMvc
    public class WebConfiguration implements WebMvcConfigurer {

        @Override
        public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            Jackson2ObjectMapperBuilder builder = 
                    new Jackson2ObjectMapperBuilder()
                    .indentOutput(true);
            converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
        }
    }
    
    public static void main(String[] args) {
		SpringApplication.run(SurfWeatherApplication.class, args);
	}

}
