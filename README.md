Dzień dobry,

Ten plik README opisuje rozwiązanie zadania programistycznego dla celów rekrutacyjnych:

# Serwis pogodowy dla windsurferów na Helu i nie tylko

### Kompilacja, budowanie i uruchamianie
Projekt **surfweather** posiada mechanizm budowania maven, który można uruchomić np. z wiersza poleceń wywołując:
```
  ./mvnw package
```

Polecenie to kompiluje pliki źródłowe programu SurfWeatherApplication razem z klasami pobocznymi i pakuje je razem z wymaganymi bibliotekami Spring Boot do jednego pliku **surfweather-0.0.1-SNAPSHOT.jar**, który można następnie uruchomić, np. poleceniem:
```
  java -jar target/surfweather-0.0.1-SNAPSHOT.jar
```

### Korzystanie
Uruchomienie programu powoduje wystartowanie lokalnego serwera http (Tomcat) nasłuchującego na porcie 8080 zawierającego aplikację **SurfWeatherApplication**.  Aplikacja ta rejestruje się na ścieżce `"/whereToSurf"` w tym serwerze i przyjmuje paramter `"date"` z datą w formacie `yyyy-mm-dd` jako wartością.  Przyjmowane są daty zarówno z przyszłości, jak i z przeszłości.

Przykładowy URL uruchamiający wygląda tak:
```
  http://localhost:8080/whereToSurf?date=2019-12-22
```

Jako wynik program przekazuje odpowiedź w formacie JSON zawierającą nazwę wygrywającej lokalizacji (`"winnerPlace"`) oraz jej warunki pogodowe (`"winnerWeather"`).
Warunki pogodowe składają się z klucza `"daily"` otrzymanego z DarkSky API (`"dsDailyJson"`) oraz wyciągniętych najważniejszych paramterów, czyli tych użytych do obliczenia najlepszego kandydata (`"windSpeed"`, `"tempLow"`, `"tempHigh"`).
Gdy program nie wyłonił żadnego kandydata klucze `"winnerPlace"` i `"winnerWeather"` są puste (odpowiednio: `""` i `null`).

### Konfiguracja
Konfiguracja miejsc do windsurfingu odbywa się poprzez plik **places.json**, który zawiera tablicę z par `{*nazwa*, *współrzędne*}` sformatowane w notacji JSON.  W przypadku braku tego pliku aplikacja uwzględnia dwie wkompilowane lokalizacje: `Jastaria` i `Chałupy`.  Domyślny plik places.json zawiera dodatkowo lokalizację `Alicante`.

### Rozważania końcowe
Warunek konieczny do zaliczenia danej lokalizacji do obliczania rankingu był zdefiniowany trochę niejednoznacznie -- temperatura w przedziale `<5; 35>` -- natomiast DarkSky API nie ma pojedynczej wartości temperatury prognozowanej, czy zmierzonej, na dany dzień tylko `"temperatureLow"` i `"temperatureHigh"` (oraz `"temperatureMin"` i `"temperatureMax"`, ale te mają chyba inne znaczenie).  
Przyjąłem zatem, że warunek ten będzie obliczany następująco:
```
  5 <= temperatureLow && temperatureHigh <= 35
```

Teraz widzę, że może lepiej byłoby zamiast temperatury zmierzonej użyć temperatury odczuwalnej (`"apparentTemperatureLow"`, `"apparentTemperatureHigh"`), ale nie wpływa to na wartość merytoryczną kodu, a jest łatwo zmienić, jeśliby ten projekt miał być użyty gdzieś produkcyjnie. :)

Pozdrawiam serdecznie,  
Mariusz Masłowski
